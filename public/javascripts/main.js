/*global $*/
function isPalindrome(status){
	var r;
	if(status === true){
		r = "Is palindrome!";
	} else {
		r = "Is not a palindrome";
	}
	document.querySelector('span.response').innerHTML = r;
}

function clearPalindrome(){
	document.querySelector('span.response').innerHTML = "";	
}

$(document).ready(function(){
   
   $('button.send').on('click', function(){
   		
	  try {
	  	clearPalindrome();
	  	
		var palindrome = document.getElementById('palindrome').value.trim(); 
		if(palindrome === ""){
			alert('You must supply a Palindrome before sending');
			return;
		}
		if(palindrome.length === 1){
			isPalindrome(true);
		}
		var endpoint = '/api/'+palindrome;
		
		$.ajax({
			url: endpoint,
			type: "GET",
			statusCode: {
				200: function (response) {
				 console.log('Status returned', '200');
					isPalindrome(true);
				},
				400: function (response) {
					console.log('Status returned', '400');
					isPalindrome(false);
				}
			}
		});
		
	  } catch(e){
		  console.error(e);
	  }
   });
});
