# API Test

1. Download from repository
	$ git clone https://maginotjunior@bitbucket.org/maginotjunior/sap-teste.git

2. Change dir 
	$ cd sap-teste/

3. Init application dependencies
	$ npm install

4. Run application
	$ npm start 

5. Access Client front End 
	http://localhost:3000/ 
	ou 
	se PORT environment variable is set 
	http://localhost:<PORT> (usualy 8080 or 8081)

6. API End Point
	http://localhost:<PORT>/api/<pandrinome>
