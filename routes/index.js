/*eslint no-new-require: "off"*/
var express = require('express');
var router = express.Router();

router.all('/', function (req, res) {
    res.redirect('home');
});

router.all('/home', function(req, res){
	res.render('index');
});

module.exports = router;