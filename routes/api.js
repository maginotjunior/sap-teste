var express = require('express');
var router = express.Router();

var debug = require('debug')('testApp'); // for debug ;)


// Api router
router.get('/', function (req, res) {
    res.status(500).end();
});

router.all('/:palindrome', function (req, res) {
	try {
		var p = (req.params['palindrome']).replace(/ /g, '').replace(/-/g, '').toLowerCase();
		var rp = p.split("").reverse().join("");
		if( rp === p){
			return res.status(200).end();
		}
		return res.status(400).end();
	} catch(e){
		debug(e);
		return res.status(500).end();
	}
});

module.exports = router;